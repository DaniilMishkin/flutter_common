import 'dart:convert';

import 'errors.dart';

class HttpDataResponse {
  static const String DATA_FIELD = 'data';
  static const String ERROR_SUB_FIELD = 'error';

  final dynamic data;

  HttpDataResponse(this.data);

  Map<String, dynamic> toJson() {
    return {DATA_FIELD: data};
  }

  factory HttpDataResponse.fromJson(Map<String, dynamic> json) {
    final data = json[DATA_FIELD];
    return HttpDataResponse(data);
  }

  bool isValid() {
    return data is Map<String, dynamic>;
  }

  ErrorJson? error() {
    if (!isValid()) {
      return null;
    }

    try {
      return ErrorJson.fromJson(data[ERROR_SUB_FIELD]);
    } catch (e) {
      return null;
    }
  }

  Map<String, dynamic>? contentMap() {
    if (!isValid()) {
      return null;
    }

    try {
      return (data as Map<String, dynamic>)[DATA_FIELD];
    } catch (e) {
      return null;
    }
  }

  List<dynamic>? contentList() {
    if (!isValid()) {
      return null;
    }

    try {
      return (data as Map<String, dynamic>)[DATA_FIELD];
    } catch (e) {
      return null;
    }
  }
}

HttpDataResponse? httpDataResponseFromString(String data) {
  try {
    return HttpDataResponse(json.decode(data));
  } catch (e) {
    return null;
  }
}
