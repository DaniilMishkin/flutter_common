import 'package:flutter/material.dart';

const BUTTON_OPACITY = 0.5;

enum ColorType { Primary, Accent }

class ListHeader extends StatelessWidget {
  const ListHeader({required this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
          const SizedBox(height: 32),
          Align(
              alignment: Alignment.bottomRight,
              child: Text(text,
                  style: TextStyle(fontSize: 14, color: Theme.of(context).colorScheme.secondary)))
        ]));
  }
}
