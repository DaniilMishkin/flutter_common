import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Common
const int KEY_UP = 19;
const int KEY_DOWN = 20;
const int KEY_LEFT = 21;
const int KEY_RIGHT = 22;

// Remote controls
const int KEY_CENTER = 23;
const int MENU = 82;
const int INFO = 165;
const int BACK = 4;
const int PREVIOUS = 89;
const int NEXT = 90;
const int PAUSE = 85;

// PC
const int ENTER = 66;
const int BACKSPACE = 67;
const int INFO_KEY = 37;
const int MENU_KEY = 41;

// Additional
const int FOCUS_ON_PROGRAMS = 0;

KeyEventResult onKey(RawKeyEvent event, KeyEventResult Function(int code) onKey) {
  if (event is RawKeyDownEvent && event.data is RawKeyEventDataAndroid) {
    final RawKeyDownEvent rawKeyDownEvent = event;
    final RawKeyEventDataAndroid rawKeyEventDataAndroid =
        rawKeyDownEvent.data as RawKeyEventDataAndroid;
    final int code = rawKeyEventDataAndroid.keyCode;
    return onKey(code);
  }
  return KeyEventResult.ignored;
}

KeyEventResult onKeyArrows(BuildContext context, RawKeyEvent event,
    {void Function()? onEnter, void Function()? onBack}) {
  return onKey(event, (key) {
    switch (key) {
      case ENTER:
      case KEY_CENTER:
        if (onEnter == null) {
          return KeyEventResult.ignored;
        }
        onEnter.call();
        return KeyEventResult.handled;
      case BACKSPACE:
      case BACK:
        if (onBack == null) {
          return KeyEventResult.ignored;
        }
        onBack.call();
        return KeyEventResult.handled;
      case KEY_UP:
        FocusScope.of(context).focusInDirection(TraversalDirection.up);
        return KeyEventResult.handled;
      case KEY_DOWN:
        FocusScope.of(context).focusInDirection(TraversalDirection.down);
        return KeyEventResult.handled;
      case KEY_RIGHT:
        FocusScope.of(context).focusInDirection(TraversalDirection.right);
        return KeyEventResult.handled;
      case KEY_LEFT:
        FocusScope.of(context).focusInDirection(TraversalDirection.left);
        return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  });
}
