import 'dart:async';

import 'package:ntp/ntp.dart';

class TimeManager {
  static TimeManager? _instance;
  int? _difference;

  static Future<TimeManager> getInstance() async {
    if (_instance == null) {
      _instance = TimeManager();
      await _instance!._setNTP();
    }
    return _instance!;
  }

  void update() async {
    await _setNTP();
  }

  Future<int> differenceMSec() async {
    if (_difference == null) {
      await _setNTP();
    }
    return _difference!;
  }

  Future<int> realTime() async {
    if (_difference == null) {
      await _setNTP();
    }
    final now = DateTime.now();
    return now.millisecondsSinceEpoch + _difference!;
  }

  // private:
  Future<void> _setNTP() async {
    try {
      final DateTime ntp = await NTP.now();
      final now = DateTime.now();
      _difference = ntp.millisecondsSinceEpoch - now.millisecondsSinceEpoch;
    } catch (e) {
      _difference = 0;
    }
  }
}
